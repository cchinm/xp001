package com.example.xp001;

import android.util.Log;

import com.google.gson.Gson;
import com.koushikdutta.async.http.server.AsyncHttpServer;
import com.koushikdutta.async.http.server.AsyncHttpServerRequest;
import com.koushikdutta.async.http.server.AsyncHttpServerResponse;
import com.koushikdutta.async.http.server.HttpServerRequestCallback;

import org.json.JSONObject;

import java.util.Scanner;

public class TestServer {
    public static String LOG_TAG = "API";

    public static void main(String[] args) {
        AsyncHttpServer server = new AsyncHttpServer();
        Scanner scanner = new Scanner(System.in);

        server.get("/testRPC", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {

                //TODO 可以这里面调用APP的东西了
                JSONObject jsonObject = new JSONObject();
                try {

                    jsonObject.put("status", true);
                    jsonObject.put("message", "Xposed RPC Demo hello world");
                    Log.d(LOG_TAG, "请求/testRPC 返回成功" + jsonObject.toString());
//                            response.send(jsonObject);
                } catch (Exception eee) {
                    Log.d(LOG_TAG, "请求/testRPC 返回错误" + eee.toString());
                } finally {
                    response.send(jsonObject);
                }
            }
        });
        server.listen(12346);
        scanner.next();
    }
}
