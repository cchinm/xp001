package com.example.xp001;

import android.util.Log;

import com.google.gson.Gson;
import com.koushikdutta.async.http.server.AsyncHttpServer;
import com.koushikdutta.async.http.server.AsyncHttpServerRequest;
import com.koushikdutta.async.http.server.AsyncHttpServerResponse;
import com.koushikdutta.async.http.server.HttpServerRequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import okhttp3.Connection;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
/*
* 得物app 版本4.69.0 hook日志练习
* 本意hook native 层进行参数打印
* 直接run app
* USB 连接手机
*
* */

public class HookDewuSign implements IXposedHookLoadPackage {

    String appVersion;
    String LOG_TAG;
    String targetClass ;
    String targetMethod;
    Object aesInstance ;
    Byte byteArr;
    Object ret;

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        appVersion = "4.69.0";
        LOG_TAG = "xp001-dewu-"+appVersion;
        targetClass = "com.duapp.aesjni.AESEncrypt";
        targetMethod = "a";
        Log.i(LOG_TAG, "appVersion: " + appVersion + " 开始进行hook");
        if (!lpparam.processName.equals("com.shizhuang.duapp") || !lpparam.packageName.equals("com.shizhuang.duapp")) {
            Log.i(LOG_TAG, "仅到该类目+"+lpparam.processName+"+"+lpparam.packageName);
            return;
        }
        Log.i(LOG_TAG,"hook \"com.shizhuang.duapp\" 开始进行搞头");
        new Thread(new Runnable() {
            @Override
            public void run() {
                XposedHelpers.findAndHookMethod("com.duapp.aesjni.AESEncrypt",
                        lpparam.classLoader,
                        "encodeByte",
                        "[B",
                        "java.lang.String",
                        new XC_MethodHook() {
                            @Override
                            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                                Log.d(LOG_TAG, "BEFORE: " + param.getResult());
                                aesInstance = param.thisObject;
                                super.beforeHookedMethod(param);

                            }

                            @Override
                            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                                Log.d(LOG_TAG, "After: " + param.getResult());
                                super.afterHookedMethod(param);

                            }
                        });
                XposedHelpers.findAndHookMethod("android.util.Log",
                        lpparam.classLoader,
                        "i",
                        "java.lang.String",
                        "java.lang.String",
                        new XC_MethodHook() {
                            @Override
                            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                                Log.d(LOG_TAG, "BEFORE: " + param.args[1].toString());
                                super.beforeHookedMethod(param);

                            }

                            @Override
                            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                                Log.d(LOG_TAG, "After: " + param.args[1].toString());
                                super.afterHookedMethod(param);

                            }
                        });
                XposedHelpers.findAndHookMethod("android.util.Log",
                        lpparam.classLoader,
                        "e",
                        "java.lang.String",
                        "java.lang.String",
                        new XC_MethodHook() {
                            @Override
                            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                                Log.d(LOG_TAG, "BEFORE: " + param.args[1].toString());
                                super.beforeHookedMethod(param);

                            }

                            @Override
                            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                                Log.d(LOG_TAG, "After: " + param.args[1].toString());
                                super.afterHookedMethod(param);

                            }
                        });
            }
        }).start();

        new Thread(() -> {
            AsyncHttpServer server = new AsyncHttpServer();

            server.get("/testRPC", new HttpServerRequestCallback() {
                @Override
                public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {

                    //TODO 可以这里面调用APP的东西了
                    JSONObject jsonObject = new JSONObject();
                    try {

                        jsonObject.put("status", true);
                        jsonObject.put("message", "Xposed RPC Demo hello world");
                        Log.d(LOG_TAG, "请求/testRPC 返回成功"+jsonObject.toString());
//                            response.send(jsonObject);
                    } catch (Exception eee) {
                        Log.d(LOG_TAG, "请求/testRPC 返回错误"+eee.toString());
                    }
                    finally {
                        response.send(jsonObject);
                    }
                }
            });

            server.get("/testDuAPP", new HttpServerRequestCallback() {
                @Override
                public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {

                    //TODO 可以这里面调用APP的东西了
                    JSONObject jsonObject = new JSONObject();
                    try {
                        Class<?> classG = XposedHelpers.findClassIfExists("com.duapp.aesjni.AESEncrypt", lpparam.classLoader);
                        Object ret = XposedHelpers.callStaticMethod(classG, "a", "123456789987654321asdfasdasdf");
                        jsonObject.put("status", true);
                        jsonObject.put("message", new Gson().toJson(ret));

                        Log.d(LOG_TAG, "请求/testDuAPP 返回成功");
                    } catch (Exception eee) {
                        try {
                            jsonObject.put("status", false);
                            jsonObject.put("message", "错误"+eee.toString());
                            Log.d(LOG_TAG, "请求/testDuAPP 返回成功"+XposedHelpers.callMethod(aesInstance, "a", "123456789789745612315456789"));
                        } catch (JSONException e) {
                            System.out.println(e.toString());
                            Log.d(LOG_TAG, "请求/testDuAPP 返回错误 "+e.toString());
                            e.printStackTrace();
                        }
                        Log.d(LOG_TAG, "请求/testDuAPP 返回错误"+eee.toString());
                    }
                    finally {
                        response.send(jsonObject);
                    }
                }
            });

            server.listen(12345);
            Log.d("xp001-server", "AsyncHttpServer start");
        }).start();

    }

}
