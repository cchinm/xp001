package com.example.xp001;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.koushikdutta.async.http.Multimap;
import com.koushikdutta.async.http.body.AsyncHttpRequestBody;
import com.koushikdutta.async.http.server.AsyncHttpServer;
import com.koushikdutta.async.http.server.AsyncHttpServerRequest;
import com.koushikdutta.async.http.server.AsyncHttpServerResponse;
import com.koushikdutta.async.http.server.HttpServerRequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.util.HashMap;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class HookTaobaoLiveSign implements IXposedHookLoadPackage {
    String appVersion;
    String LOG_TAG;
    String targetClass ;
    String targetMethod;
    Object chainInstance ;
    Object classInstance;
    final Gson gson = new Gson();


    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        appVersion = "1.8.6-tblive";
        LOG_TAG = "xp001-"+appVersion;
        targetClass = "mtopsdk.security.InnerSignImpl";
        String appClass = "com.taobao.live";
        targetMethod = "a";
        String ApiName = "/rpc/testTaobaoLive";
        Log.i(LOG_TAG, "appVersion: " + appVersion + " 开始进行hook");
        if (!lpparam.processName.equals("com.taobao.live") || !lpparam.packageName.equals("com.taobao.live")) {
            Log.i(LOG_TAG, "仅到该类目+"+lpparam.processName+"+"+lpparam.packageName);
            return;
        }
        Log.i(LOG_TAG,"hook \""+appClass+"\" 开始进行搞头");

        new Thread(new Runnable() {
            @Override
            public void run() {
                XposedHelpers.findAndHookMethod(targetClass, lpparam.classLoader, "getUnifiedSign", "java.util.HashMap", "java.util.HashMap", "java.lang.String", "java.lang.String", boolean.class, new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//                        XposedBridge.log("查找到Class. 开始执行: ");
                        Log.i(LOG_TAG, "查找到Class. 开始执行: "+targetClass);
                        classInstance = param.thisObject;
                        super.afterHookedMethod(param);

                    }
                });

            }
        }).start();


        new Thread(() -> {
            AsyncHttpServer server = new AsyncHttpServer();

            server.get("/testRPC", new HttpServerRequestCallback() {
                @Override
                public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {

                    //TODO 可以这里面调用APP的东西了
                    JSONObject jsonObject = new JSONObject();
                    try {

                        jsonObject.put("status", true);
                        jsonObject.put("message", "Xposed RPC Demo hello world");
                        Log.d(LOG_TAG, "请求/testRPC 返回成功"+jsonObject.toString());
//                            response.send(jsonObject);
                    } catch (Exception eee) {
                        Log.d(LOG_TAG, "请求/testRPC 返回错误"+eee.toString());
                    }
                    finally {
                        response.send(jsonObject);
                    }
                }
            });

            server.get(ApiName, new HttpServerRequestCallback() {
                @Override
                public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
//                    String params = (String) request.getBody().get();
//                    HashMap map = gson.fromJson(params, HashMap.class);
//                    Log.i(LOG_TAG, map.toString());
                    //TODO 可以这里面调用APP的东西了
                    JSONObject jsonObject = new JSONObject();
                    try {
//                        Class<?> classG = XposedHelpers.findClassIfExists(targetClass, lpparam.classLoader);
                        HashMap<String , String> hashMap = new HashMap<String, String>();;
                        HashMap<String , String> hashMap2 = new HashMap<String, String>();;
                        /**
                         * {data={"tab":"2","limit":"20","topic":"8f5e3212-7a21-4062-bede-bd8f71a38a65","paginationContext":"{\"commentId\":317222327741,\"refreshTime\":1625125932450,\"timestamp\":1625125929151}",
                         *     "neoRoomType":"0","order":"asc"},
                         *     deviceId=Anw2bYobgNz4JVILFsWarOGjx4q8zB226EICHArxNV8r,
                         *     sid=18f8ff7fec5562f988c7631f1e1502be,
                         *     uid=2206537109033,
                         *     x-features=27,
                         *     appKey=25443018,
                         *     api=mtop.taobao.iliad.comment.query.latest,
                         *     utdid=X/gFGoQO7JsDAMdkB14Q4IDk,
                         *     ttid=700407@taobaolive_android_1.8.6,
                         *      t=1625125937, v=1.0}
                         */

                        hashMap.put("data", "{\"tab\":\"2\",\"limit\":\"20\",\"topic\":\"8f5e3212-7a21-4062-bede-bd8f71a38a65\",\"paginationContext\":\"{\\\"commentId\\\":317222327741,\\\"refreshTime\\\":1625125932450,\\\"timestamp\\\":1625125929151}\",\"neoRoomType\":\"0\",\"order\":\"asc\"}");
                        hashMap.put("deviceId", "Anw2bYobgNz4JVILFsWarOGjx4q8zB226EICHArxNV8r");
                        hashMap.put("sid", "18f8ff7fec5562f988c7631f1e1502be");
                        hashMap.put("uid", "2206537109033");
                        hashMap.put("x-features", "27");
                        hashMap.put("appKey", "25443018");
                        hashMap.put("api", "mtop.taobao.iliad.comment.query.latest");
                        hashMap.put("utdid", "X/gFGoQO7JsDAMdkB14Q4IDk");
                        hashMap.put("ttid", "700407@taobaolive_android_1.8.6");
                        hashMap.put("t", "1625125937");
                        hashMap.put("v", "1.0");
                        //{pageId=https://market.m.taobao.com/app/mtb/app-tblive-room/pages/index2?wh_weex=true,
                        //pageName=market.m.taobao.com/app/mtb/app-tblive-room/pages/index2}

                        hashMap2.put("pageId", "https://market.m.taobao.com/app/mtb/app-tblive-room/pages/index2?wh_weex=true");
                        hashMap2.put("pageName", "market.m.taobao.com/app/mtb/app-tblive-room/pages/index2");



                        Object ret = XposedHelpers.callMethod(classInstance, "getUnifiedSign", hashMap, hashMap2, "25443018", null, false);

                        jsonObject.put("status", true);
                        jsonObject.put("message", new Gson().toJson(ret));

                        Log.d(LOG_TAG, "请求"+ApiName+" 返回成功");
                    } catch (Exception eee) {
                        try {
                            jsonObject.put("status", false);
                            jsonObject.put("message", "错误"+eee.toString());
                            Log.d(LOG_TAG, "请求"+ApiName+" 返回成功");
                        } catch (JSONException e) {
                            System.out.println(e.toString());
                            Log.d(LOG_TAG, "请求"+ApiName+" 返回错误 "+e.toString());
                            e.printStackTrace();
                        }
                        Log.d(LOG_TAG, "请求"+ApiName+" 返回错误"+eee.toString());
                    }
                    finally {
                        response.send(jsonObject);
                    }
                }
            });

            server.get("/get/sign", new HttpServerRequestCallback() {
                @Override
                public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {

                    JSONObject jsonObject = new JSONObject();

                    try {
                        String stringMap = request.getQuery().getString("hashMap") ;
                        Log.i(LOG_TAG, stringMap);
                        String stringMap2 = request.getQuery().getString("hashMap2") ;
                        Log.i(LOG_TAG, stringMap2);
                        String appKey = request.getQuery().getString("appKey") ;
                        Log.i(LOG_TAG, appKey);
                        HashMap hashMap2 = gson.fromJson(stringMap2, HashMap.class);
                        HashMap hashMap = gson.fromJson(stringMap, HashMap.class);
                        HashMap<String , String> h = new HashMap<String, String>();
                        HashMap<String , String> h2 = new HashMap<String, String>();
                        HashMap<String, Object> result = new HashMap<>();

                        for (Object k: hashMap.keySet()){
                            h.put((String)k , (String) hashMap.get(k));
                        }
                        for (Object k: hashMap2.keySet()){
                            h2.put((String)k , (String) hashMap2.get(k));
                        }

                        Log.i(LOG_TAG, h.toString());
                        Log.i(LOG_TAG, h2.toString());

//                        String str = (String) dataMap.get("str");
//                        Boolean z = (Boolean) dataMap.get("z");
//                        Log.i(LOG_TAG, "获取参数:" + dataMap.toString());
                        Object ret = XposedHelpers.callMethod(classInstance,
                                "getUnifiedSign",
                                h,
                                h2,
                                appKey,
                                null,
                                false);

//                        JSONObject jsonObject = new JSONObject();
                        result.put("hashMap", hashMap);
                        result.put("hashMap2", hashMap2);
                        result.put("appkey", appKey);
                        result.put("result", ret);
                        jsonObject.put("status", true);
                        jsonObject.put("message", gson.toJson(result));

                    } catch (Exception e) {
                        JSONObject json = new JSONObject();
                        try {
                            json.put("code", false);
                            json.put("message", e.toString());
                        } catch (JSONException ee) {
                            ee.printStackTrace();
                        }
//                        json.put("message")
//                        response.send(json);

                    } finally {
                        response.send(jsonObject);
                    }
//

                }
            });


            server.listen(12346);
            Log.d(LOG_TAG, "AsyncHttpServer start");
        }).start();

    }
}
