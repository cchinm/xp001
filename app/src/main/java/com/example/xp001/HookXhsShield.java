package com.example.xp001;

import android.util.Log;

import com.google.gson.Gson;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class HookXhsShield implements IXposedHookLoadPackage {

    String appVersion;
    String LOG_TAG;
    String targetClass ;
    String targetMethod;
    Object chainInstance ;


    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {

        appVersion = "6.cc.cc";
        LOG_TAG = "xp001-xhs-"+appVersion;
        targetClass = "com.xingin.shield.http.RedHttpInterceptor";
        String appClass = "com.xingin.xhs";
        targetMethod = "a";
        Log.i(LOG_TAG, "appVersion: " + appVersion + " 开始进行hook");
        if (!lpparam.processName.equals(appClass) || !lpparam.packageName.equals("com.xingin.xhs")) {
            Log.i(LOG_TAG, "仅到该类目+"+lpparam.processName+"+"+lpparam.packageName);
            return;
        }
        Log.i(LOG_TAG,"hook \""+appClass+"\" 开始进行搞头");
        XposedHelpers.findAndHookMethod(targetClass,
                lpparam.classLoader,
                "process",
                "okhttp3.Interceptor$Chain",
                new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        Log.d(LOG_TAG, "BEFORE: " + param.getResult());
                        chainInstance = param.thisObject;
                        Log.d(LOG_TAG, new Gson().toJson(chainInstance));
                        super.beforeHookedMethod(param);

                    }

                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        Log.d(LOG_TAG, "After: " + param.getResult());
                        super.afterHookedMethod(param);

                    }
                });
    }
}
